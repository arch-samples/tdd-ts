import { expect,assert } from 'chai';
import { Lead } from "./Lead";
import { LeadEntity } from "./LeadEntity";

describe('TDD :: TYPESCRIPT :: SAMPLE', () => {
    let tested: Lead;

    /*
        before()     :: Executado uma vez antes de todos os testes (IT) do describe
        after()      :: Executado uma vez após todos os testes  (IT) do describe
        beforeEach() :: Executado antes de cada teste (IT) do describe
        afterEach()  :: Executado após cada teste (IT) do describe
    */

    beforeEach(() => tested = new Lead());

    it("Add one lead", () => {
        const result = tested.add("carlos", "pisani@archoffice.teh", "(11) 9.9696-9696");
        expect(result).to.equal(true);
    });

    it("Get one lead", () => {
        const result = tested.get("pisani@archoffice.teh");
        expect(result).be.an.instanceOf(LeadEntity);
        expect(result).to.have.property('name').to.equal('carlos');
        expect(result).to.have.property('email').to.equal('pisani@archoffice.teh');
        expect(result).to.have.property('phone').to.equal('(11) 9.9696-9696');
    });

    it("Get all leads", () => {
        const result = tested.getAll();
        expect(result).be.an.instanceOf(Array).and.have.lengthOf(1);
    });

    it("Delete lead", () => {
        const result = tested.del("pisani@archoffice.teh");
        expect(result).to.equal(true);
    });

});
